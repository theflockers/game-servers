# Creates a Minecraft server

### Requirements
- A running  Debian GNU/Linux (might work on Ubuntu as well) box

@todo: terraform files

### running

- clone the repo;

```
~ $ cd game-servers
~/game-servers $ cd ansible
~/ansible $ ansible-playbook -i <server>, -u <user> mc-server.yml
```
